from setuptools import setup
from setuptools import find_packages

setup(
    name='toggle_proxy',
    version='0.4.2',
    description='Python script that toggles various proxy settings throughout the system.',
    url='https://github.ford.com/mruss100/toggle_proxy',
    author='Matthew Russell',
    author_email='mruss100@ford.com',
    license='MIT',
    packages=find_packages(),
    install_requires=['dnspython'],
    python_requires='>=3.6',
    entry_points = {
      'console_scripts': [
          'toggle-proxy = toggle_proxy.tp:main',
          'proxy-on = toggle_proxy.tp:activate',
          'proxy-off = toggle_proxy.tp:deactivate',
          'install-proxy-toggler = toggle_proxy.installer:main',
      ]
    },
    zip_safe=False
)
