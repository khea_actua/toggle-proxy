# Overview

Install small to toggle system and user proxies in Linux environments.

Supports:
- apt
- git
- docker
- env (prints env variables to input)
- gradle

Was written with the goal of use both on Linux host machines as well as in a
container (_i.e._ when run in a docker container, it won't attempt to modify
docker settings.)

Configuration is still hard-coded (see comments below).

# Install

You'll likely want a root and user install.

```sh
# User install
pip3 install git+https://gitlab.com/khea_actua/toggle-proxy.git#egg=toggle_proxy

# System install
sudo pip3 install git+https://gitlab.com/khea_actua/toggle-proxy.git#egg=toggle_proxy
```

The reason I recommend `sudo` over the usual recommendation I would make of
installing to a virtual environment is because this script should be run as
root in order to modify some system settings such as `apt` and the `docker`
service.  I suppose an alternative would be to install to a virtualenv and
modify the execution bit to run as root - I have not tested this though.

To configure, fill out the following into `/etc/proxy-toggle.json`:
```json
{
  "backup_ip":   "backup IP if we cannot resolve proxy host name",
    "host":      "proxy host name",
    "port":      "proxy port number (int)",
    "no_proxy":  "Comma delimeted list of hostnames/ips to skip",
    "dns": {
      "nameservers": [
        "ns1 ip",
        "ns2 ip"
      ],
      "search": "search domain"
    }
}
```

Again, because this app modifies system settings, and can be configured to be
run with sudo without a password, it's not desirable for the user to be able to
specify a particular configuration file.

# Usage

All commands are parsed can be run with `--help` to see the command options.
The toggling commands can take a list of services (which defaults to _all_, if
none of the services modify the system then you can run them without sudo.  If
you run this as a normal user and require sudo, the program will simply output
the steps you need to take to accomplish modifying the setting.

The commands also suppose a `run as` argument.  This is important when running
as root but also wanting to modify your git config, for example.  Without
specifying `run as`, you will modify root's `gitconfig` rather than your own.

## `proxy-on` and `proxy-off` examples

- Enable docker service, docker client, apt, git, _etc_ as your user:
```sh
sudo proxy-on -r ${USER}
```
- Enable only git and the docker client:
```sh
sudo -H proxy-on -r $(whoami) dockerclient git
```
- Disable apt 
```sh
sudo proxy-off apt
```
- Inspect what the script is doing without affecting anything (dry-run)
```sh
sudo proxy-on -n
```

## `install-proxy-toggler`

This command enables the user to invoke the proxy toggling commands with sudo
without having to enter a password (not relevant if used inside vagrant VMs or
docker containers where the default already doesn't require passwords.)

```sh
sudo install-proxy-toggler
```

This is done by creating the file `/etc/sudoers.d/toggle_proxy`.

Originally this app was a simple executable script, so for safety (user script
being executed by root concerns) this install command configures sudo to only
allow this app password-free if the script hash matches what it was when
installed.  Now that it's been converted to an installable package however this
check has to be applied on all the commands, but doesn't work as well since the
commands are simply short front-ends to the actual code where the config is
kept.  However, this hash check doesn't cause any issues so I left it in.

# Comments

- My preferred usage is with environmental modules, see [my proxy module file
  here](https://github.ford.com/MRUSS100/modulefiles/blob/master/proxy)
- Because of security concerns early, I hard coded the configuration, however
  the next revision will update this to be able to load a config file.

# Development

To install locally using pip (such that it can be cleanly uninstalled), run:
```sh
pip install --no-index --find-links <package-path> <package-path>
```

For example, from the directory `/home/matt/workspace/toggle-proxy`, run:
```sh
pip install --no-index --find-links $(pwd) .
```

[modeline]: # ( vim: set fenc=utf-8 spell spl=en ts=2 sw=2 expandtab sts=0 ff=unix : )
