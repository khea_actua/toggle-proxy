#!/usr/bin/env python3

from __future__ import print_function

import argparse
import os
import sys

try:
    import toggle_proxy.helpers as helpers
    import toggle_proxy.st as st
except:
    import helpers
    import st


def toggle_proxy(activate=True, services=st.ServicesEnum.ALL, run_as_uid=None, dryrun=False, log_level=False):
    proxy_settings = helpers.get_proxy_settings(add_ip_if_needed = activate)
    pt = st.ProxyToggler(
        settings=proxy_settings,
        run_as_uid=run_as_uid,
        activate=activate,
        dryrun=dryrun,
        log_level=log_level,
    )
    pt.run_togglers(services)

def main():
    parser = argparse.ArgumentParser(
        prog='toggle_proxy',
        description='Toggle Proxy'
    )
    group_op  = st.get_op_argparser(parser)

    group_op.add_argument(
        '-a', '--activate',
        dest='activate',
        action='store_true',
        help='Activate proxy'
    )

    group_op.add_argument(
        '-d', '--deactivate',
        dest='deactivate',
        action='store_false',
        help='Deactivate proxy'
    )

    args = parser.parse_args()

    toggle_proxy(activate=args.activate, services=args.services, dryrun=args.dryrun)


def activate():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(sys.argv[0]),
        description='Activate the proxy'
    )
    group_op = st.get_op_argparser(parser)
    args = parser.parse_args()

    toggle_proxy(True, args.services, run_as_uid=args.run_as, dryrun=args.dryrun)

def deactivate():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(sys.argv[0]),
        description='Deactivate the proxy'
    )
    group_op = st.get_op_argparser(parser)
    args = parser.parse_args()

    toggle_proxy(False, args.services, run_as_uid=args.run_as, dryrun=args.dryrun)

if __name__ == "__activate__":
    activate()
elif __name__ == "__deactivate__":
    deactivate()
elif __name__ == "__main__":
    main()

# vim: ts=4 sw=4 sts=0 expandtab ff=unix :
