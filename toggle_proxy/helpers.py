from __future__ import print_function

import json
import pwd
import re
import os
import argparse
import dns.resolver


class Ansi:
    act      = '\u001b[38;5;%dm'%184
    deact    = '\u001b[38;5;%dm'%200
    error    = '\u001b[38;5;%dm'%196
    warn     = '\u001b[38;5;%dm'%226
    info     = '\u001b[38;5;%dm'%112
    alt      = '\u001b[38;5;%dm'%110
    dryrun   = '\u001b[38;5;%dm'%202
    clear    = '\u001b[0m'
    bold     = '\u001b[1m'
    reversed = '\u001b[7m'

def get_proxy_settings(add_ip_if_needed):
    conf_file = os.path.join('/etc', 'proxy-toggle.json')
    if not os.path.exists(conf_file):
        err_msg = 'Could not find config file %s\nPlease create the required JSON configuration with the format:\n%s'%(conf_file, json.dumps({
            'backup_ip': 'backup IP if we cannot resolve proxy host name',
            'host': 'proxy host name',
            'port': 'proxy port number (int)',
            'no_proxy': 'Comma delimeted list of hostnames/ips to skip',
            'dns': {
                'nameservers': ['ns1 ip', 'ns2 ip'],
                'search': 'search domain'
                }
            }, indent=2))
        raise IOError(err_msg)
    else:
        with open(conf_file) as f:
            settings = json.load(f)

    # If the IP isn't in proxy_settings, do a DNS lookup to find it and add it
    # to the settings
    if not 'ip' in settings:
        try:
            answers = dns.resolver.query(settings['host'])
            if len(answers):
                settings['ip'] = answers[0]
                print('Resolved %s for proxy IP'%settings['ip'])
        except:
            settings['ip'] = settings['backup_ip']
            print('Could not resolve %s, using %s'%(settings['host'], settings['backup_ip']))

    return settings

def argparse_uid(user):
    """ Provide the UID of the user or UID provided """

    if user is None:
        return None

    uid = None
    if not re.match('^\d+$', user):
        uid = pwd.getpwnam(user).pw_uid
    else:
        uid = user

    # raise argparse.ArgumentTypeError('Could not match user '%s': %s'%(user, e))

    return uid


def in_docker():
    """ Test to see if we're in a docker container """
    return os.path.exists('/.dockerenv')

# vim: ts=4 sw=4 sts=0 expandtab ff=unix :
