# Module st (service toggles)

from __future__ import print_function

import shutil
import platform
import subprocess
import enum
import configparser
import json
import pwd
import re
import sys
import io
import os

try:
    import toggle_proxy.helpers as helpers
except:
    import helpers

def get_op_argparser(parser):
    """ Return a group parser of command line arguments

    I would prefer this to be in Helpers, but that introduces a circular dependance
    """

    group = parser.add_argument_group()

    group.add_argument(
        '-n', '--dryrun',
        dest='dryrun',
        action='store_true',
        help='Dry run'
    )

    group.add_argument(
        '-l', '--log-level',
        dest='log_level',
        action='store',
        type=LogLevelEnum.argparse,
        choices=tuple(LogLevelEnum),
        default=(LogLevelEnum.INFO),
        help='Log Level  (default: %(default)s)'
    )

    group.add_argument(
        '-r', '--run-as',
        dest='run_as',
        action='store',
        default=None,
        type=helpers.argparse_uid,
        help='Run non-system toggles as this user'
    )

    group.add_argument(
        dest='services',
        action='store',
        nargs='*',
        type=ServicesEnum.argparse,
        choices=tuple(ServicesEnum),
        default=(ServicesEnum.ALL),
        help='List services to reload  (default: %(default)s)'
    )

    return group

class LogLevelEnum(enum.IntEnum):
    DEBUG = 1
    INFO  = 2
    WARN  = 3
    ERROR = 4

    def __str__(self):
        return self.name.lower()

    def __repr__(self):
        return str(self)

    @staticmethod
    def argparse(s):
        try:
            return LogLevelEnum[s.upper()]
        except KeyError:
            return s


class ServicesEnum(enum.IntEnum):
    NONE          = 1 # hack for default arg
    ALL           = 2 # hack for all
    DOCKERSERVICE = 3
    DOCKERCLIENT  = 4
    GIT           = 5
    APT           = 6
    ENV           = 7
    GRADLE        = 8

    def __str__(self):
        return self.name.lower()

    def __repr__(self):
        return str(self)

    @staticmethod
    def argparse(s):
        try:
            return ServicesEnum[s.upper()]
        except KeyError:
            return s

def demote(user_uid, user_gid):
    """Pass the function 'set_ids' to preexec_fn, rather than just calling
    setuid and setgid. This will change the ids for that subprocess only"""

    def set_ids():
        os.setgid(user_gid)
        os.setuid(user_uid)

    return set_ids

def get_demote_function(run_as_uid, service, is_super=False):
    """ Little helper function to get the proper pre-exec function for
    demotting permissions.  It also outputs appropriate warnings """

    if run_as_uid is None and is_super:
        print('Warning: toggling the %s proxy should be done by a non-root user'%service)
        preexec_fn = None
    elif run_as_uid is not None and is_super:
        preexec_fn = demote(run_as_uid, run_as_uid)
    elif run_as_uid is not None and not is_super:
        print('Warning: attempting to toggling %s proxy as a different user without root access'%service)
        preexec_fn = demote(run_as_uid, run_as_uid)
    else:
        preexec_fn = None

    return preexec_fn

class ProxyToggler:
    def __init__(
        self,
        settings=ServicesEnum.ALL,
        run_as_uid=None,
        activate=True,
        dryrun=False,
        log_level=LogLevelEnum.INFO,
    ):
        self.settings   = settings
        self.run_as_uid = run_as_uid
        self.activate   = activate
        self.dryrun     = dryrun
        self.is_super   = 0 == os.getuid()
        self.in_docker  = helpers.in_docker()
        self.log_level  = log_level

        self.use_ansi = True

        if self.run_as_uid is None:
            self.home_dir = os.environ.get('HOME')
        else:
            self.home_dir = pwd.getpwuid(self.run_as_uid).pw_dir

        if 'root' in self.home_dir:
            self.print_warn('Home directory set to %s, are you sure you didn\'t mean to run this with "sudo -H"?  Without -H user settings on root will be adjusted.'%self.home_dir)

    def print_status(self, msg):
        print('%s%s%s'%(helpers.Ansi.act if self.activate else helpers.Ansi.deact, msg, helpers.Ansi.clear))
    def print_error(self, msg):
        if self.log_level >= LogLevelEnum.ERROR:
            print('%s%s%s'%(helpers.Ansi.error, msg, helpers.Ansi.clear), file=sys.stderr)
    def print_warn(self, msg):
        if self.log_level >= LogLevelEnum.WARN:
            print('%s%s%s'%(helpers.Ansi.warn, msg, helpers.Ansi.clear))
    def print_info(self, msg):
        if self.log_level >= LogLevelEnum.INFO:
            print('%s%s%s'%(helpers.Ansi.info, msg, helpers.Ansi.clear), file=sys.stderr)
    def print_debug(self, msg):
        if self.log_level >= LogLevelEnum.DEBUG:
            print('%s%s%s'%(helpers.Ansi.debug, msg, helpers.Ansi.clear))
    def print_alt(self, msg):
        print('%s%s%s'%(helpers.Ansi.alt, msg, helpers.Ansi.clear))
    def print_dryrun(self, msg):
        print('%s%sWHATIF%s%s: %s%s'%(helpers.Ansi.dryrun, helpers.Ansi.reversed, helpers.Ansi.clear, helpers.Ansi.dryrun, msg, helpers.Ansi.clear))


    def toggle_git(self):
        """ Simply calls git config """

        if not shutil.which('git'):
            self.print_info('Git not installed, skipping')
            return

        preexec_fn = get_demote_function(self.run_as_uid, service='git', is_super=self.is_super)

        cmd = ['/usr/bin/git', 'config', '--global']
        env=None
        if self.run_as_uid is not None:
            env = {'HOME': self.home_dir}
        if self.activate:
            proxy_str='http://%s:%d'%(self.settings['host'], self.settings['port'])
            self.print_status('Activating proxy (%s) for git'%(proxy_str))
            cmd.append('http.proxy')
            cmd.append(proxy_str)
        else:
            self.print_status('Deactivating proxy for git')
            cmd.append('--unset')
            cmd.append('http.proxy')

        if self.dryrun:
            self.print_dryrun('Issuing command: %s'%' '.join(cmd))
        else:
            res = subprocess.run(cmd, env=env)

    def toggle_gradle(self):
        """ Simply calls git config """

        fname=os.path.join(self.home_dir, '.gradle', 'gradle.properties')

        config_file_existed = False
        if os.path.exists(fname):
            config_file_existed = True
            with open(fname) as f:
                content = [line.strip() for line in f]
        else:
            # Ensure the directory at least exists
            if not os.path.exists(os.path.dirname(fname)):
                os.makedirs(os.path.dirname(fname))
            content = []

        # Remove any proxy keys
        new_content = []
        for l in content:
            m = re.match('^systemProp.http(s|).(nonProxyHosts|proxyHost|proxyPort).*', l)
            if not m:
                new_content.append(l)
        content = new_content

        if self.activate:
            self.print_status('Activating proxy for gradle client in %s'%(fname))
            new_content = []
            new_content.append('systemProp.http.nonProxyHosts=%s'%self.settings['no_proxy'])
            new_content.append('systemProp.http.proxyHost=%s'%self.settings['host'])
            new_content.append('systemProp.http.proxyPort=%s'%self.settings['port'])
            new_content.append('systemProp.https.nonProxyHosts=%s'%self.settings['no_proxy'])
            new_content.append('systemProp.https.proxyHost=%s'%self.settings['host'])
            new_content.append('systemProp.https.proxyPort=%s'%self.settings['port'])

            if self.dryrun:
                self.print_dryrun('Add the following to %s\n%s'%(fname, '\n'.join(new_content)))
            else:
                with open(fname, 'w+') as f:
                    f.write('\n'.join(content) + '\n' + '\n'.join(new_content))

        else:
            self.print_status('Deativating proxy for gradle client in %s'%(fname))

            if self.dryrun:
                self.print_dryrun('Remove systemProp.http(s|).(nonProxyHosts|proxyHost|proxyPort) jeys from %s'%fname)
            else:
                with open(fname, 'w') as f:
                    f.write('\n'.join(content))


    def toggle_apt(self):
        if 'Windows' == platform.system():
            self.print_info('Apt not supported on Windows, skipping')
            return
        if not os.path.exists('/etc/apt'):
            self.print_info('Apt not installed, skipping')
            return

        fname = '/etc/apt/apt.conf.d/95-proxy.conf'

        if self.activate:
            proxy_str='%s:%d'%(self.settings['host'], self.settings['port'])
            self.print_status('Activating proxy (%s) for apt in %s'%(proxy_str, fname))
            if 'cloud' in self.settings and isinstance(self.settings['cloud'], list) and len(self.settings['cloud']):
                http_proxy='{\n%s\n}'%('\n'.join("   " + s + " DIRECT;" for s in self.settings['cloud']))
            else:
                http_proxy='http://{proxy}'.format(proxy=proxy_str)

            contents = '''Acquire::http::Proxy "http://{proxy}";
Acquire::https::Proxy "http://{proxy}";
Acquire::ftp::Proxy "ftp://{proxy}";
Acquire::http::Proxy {http_proxy}
DPkg::Options {{ "--force-confdef"; "--force-confold"; }}
'''.format(proxy=proxy_str, http_proxy=http_proxy)

            if self.is_super and not self.dryrun:
                with open(fname, 'w+') as f:
                    f.write(contents)
            else:
                msg = 'Write the following into %s:\n%s'%(fname, contents)
                if self.dryrun:
                    self.print_dryrun(msg)
                else:
                    self.print_alt(msg)
        else:
            self.print_status('Deativating proxy for apt in %s'%(fname))
            if os.path.exists(fname):
                contents = '''Acquire::http::Proxy "false";
Acquire::https::Proxy "false";
Acquire::ftp::Proxy "false";
'''
                if self.is_super:
                    with open(fname, 'w') as f:
                        f.write(contents)
                else:
                    msg = 'Write the following into %s:\n%s'%(fname, contents)
                    if self.dryrun:
                        self.print_dryrun(msg)
                    else:
                        self.print_alt(msg)


    def toggle_docker_client(self):
        if not shutil.which('docker'):
            self.print_info('Docker client not installed, skipping')
            return

        if 'DOCKER_CONFIG' in os.environ:
            docker_path = os.environ['DOCKER_CONFIG']
        else:
            docker_path = os.path.join(self.home_dir, '.docker')
        if not os.path.exists(docker_path):
            self.print_error('Docker client configuration path %s does not exist, please either create it or specify your desired path with the DOCKER_CONFIG environment variable'%docker_path)
            return

        fname=os.path.join(docker_path, 'config.json')

        config_file_existed = False
        if os.path.exists(fname):
            config_file_existed = True
            self.print_debug('Reading %s'%fname)
            with open(fname) as f:
                data = json.load(f)
        else:
            self.print_debug('%s does not exist'%fname)
            data = {}

        if self.activate:
            proxy_str='http://%s:%d'%(self.settings['ip'], self.settings['port'])
            self.print_status('Activating proxy (%s) for docker client in %s'%(proxy_str, fname))
            data['proxies'] = {
                'default': {
                    'httpProxy':  proxy_str,
                    'httpsProxy': proxy_str,
                    'noProxy': '{no_proxy}'.format(no_proxy=self.settings['no_proxy']),
                }
            }
        else:
            self.print_status('Deativating proxy for docker client in %s'%(fname))
            if 'proxies' in data:
                del data['proxies']

        if not len(data.keys()) and os.path.exists(fname):
            if self.dryrun:
                self.print_dryrun('Delete %s'%fname)
            else:
                os.remove(fname)
        else:
            if self.dryrun:
                self.print_dryrun('Write the following to %s:\n%s'%(fname, json.dumps(data, indent=2)))
            else:
                self.print_debug('Writing data to %s'%fname)
                with open(fname, 'w') as f: json.dump(data, f, indent=2)


    def toggle_docker_system(self):
        if not os.path.exists('/usr/bin/dockerd'):
            self.print_info('Docker service not installed, skipping')
            return

        #
        # Modify docker service proxy conf
        # https://docs.docker.com/config/daemon/systemd/

        if not self.is_super:
            self.print_warn('Script must be run as root')

        fname='/etc/systemd/system/docker.service.d/10_docker_proxy.conf'
        if not os.path.exists(os.path.dirname(fname)):
            os.makedirs(os.path.dirname(fname))

        config = configparser.ConfigParser()
        config.read(fname)
        if self.activate:
            proxy_str='http://%s:%d'%(self.settings['host'], self.settings['port'])
            self.print_status('Activating proxy (%s) for docker service in %s'%(proxy_str, fname))
            if not config.has_section('Service'):
                config.add_section('Service')
            config['Service']['Environment'] = '"HTTP_PROXY={proxy}" "HTTPS_PROXY={proxy}" "NO_PROXY={no_proxy}"'.format(proxy=proxy_str, no_proxy=self.settings['no_proxy'])
        else:
            self.print_status('Deactivating proxy for docker service in %s'%(fname))
            if config.has_option('Service', 'environment'):
                del config['Service']['environment']

        buf = io.StringIO()
        config.write(buf)
        contents = buf.getvalue()

        # Fix the case of the keys
        contents = re.sub(r'^environment\s*=\s*', 'Environment=', contents, flags=re.MULTILINE)

        if not self.is_super:
            self.print_alt('Cannot write config witout root permission, write the following to %s\n%s'%(fname, contents))
        else:
            if self.dryrun:
                self.print_dryrun('Write the following to %s:\n%s'%(fname, contents))
            else:
                with open(fname, 'w') as f: f.write(contents)

        #
        # Modify daemon.json
        fname='/etc/docker/daemon.json'

        config_file_existed = False
        if os.path.exists(fname):
            config_file_existed = True
            with open(fname) as f:
                data = json.load(f)
        else:
            data = {}

        if self.activate:
            self.print_status('Activating DNS nameservers for docker service in %s'%(fname))
            data['dns'] = self.settings['dns']['nameservers']
        else:
            self.print_status('Deativating DNS nameservers for docker service in %s'%(fname))
            if 'dns' in data:
                del data['dns']

        if not len(data.keys()) and os.path.exists(fname):
            if not self.is_super:
                self.print_alt('Cannot remove config without root permission, remove the following file: %s'%fname)
            else:
                if self.dryrun:
                    self.print_dryrun('Delete %s'%fname)
                else:
                    os.remove(fname)
        else:
            if not self.is_super:
                self.print_alt('Cannot write config without root permission, write the following to: %s\n\n%s'%(fname, json.dumps(data, indent=2)))
            else:
                if self.dryrun:
                    self.print_dryrun('Write the following to %s:\n%s'%(fname, json.dumps(data, indent=2)))
                else:
                    with open(fname, 'w') as f: json.dump(data, f, indent=2)

        #
        # Restart docker service

        if not self.is_super:
            self.print_alt('''Issue the following commands as root to flush and restart docker with:

sudo systemctl daemon-reload
sudo systemctl restart docker''')
        else:
            if self.dryrun:
                self.print_dryrun('Run:\n/usr/bin/systemctl daemon-reload\n/usr/bin/systemctl restart docker')
            else:
                # Probably a better way to do this:
                os.system('systemctl daemon-reload')
                os.system('systemctl restart docker')


    def toggle_env(self):
        """ Doesn't really do anything other than message the user """
        env = os.environ

        alt_str = []
        if self.activate:
            if 'Linux' == platform.system():
                proxy_str='http://%s:%d'%(self.settings['host'], self.settings['port'])
                for s in 'http_proxy', 'https_proxy', 'HTTP_PROXY', 'HTTPS_PROXY':
                    if s not in env or proxy_str != env[s]:
                        alt_str.append('export {var}={proxy}'.format(var=s, proxy=proxy_str))
                for s in 'no_proxy', 'NO_PROXY':
                    if s not in env or self.settings['no_proxy'] != env[s]:
                        alt_str.append('export {var}={val}'.format(var=s, val=self.settings['no_proxy']))
                if len(alt_str):
                    self.print_alt('Execute the following to install the proxy into your current environment:\n\n%s'%'\n'.join(alt_str))
            else:
                # alt_str.append('Pwoershell: Install Set-InternetProxy.ps1\nSet-InternetProxy -Proxy %s'%proxy_str)
                pass
        else:
            if 'Linux' == platform.system():
                for s in 'http_proxy', 'https_proxy', 'HTTP_PROXY', 'HTTPS_PROXY', 'no_proxy', 'NO_PROXY':
                    if s in env:
                        alt_str.append('unset {var}'.format(var=s))
            else:
                # alt_str.append('Pwoershell: Install Set-InternetProxy.ps1\nSet-InternetProxy -Proxy %s'%proxy_str)
                pass

            if len(alt_str):
                self.print_alt('Execute the following to uninstall the proxy into your current environment:\n%s'%'\n'.join(alt_str))


    def run_togglers(self, services):
        # Readability shortcut
        if ServicesEnum == type(services):
            services = [services]

        do_all_except_docker = (ServicesEnum.ALL in services) and not self.in_docker
        do_all               = ServicesEnum.ALL in services

        if self.run_as_uid is not None:
            self.print_info('Toggling proxy as %s'%pwd.getpwuid(self.run_as_uid).pw_name)

        if ServicesEnum.DOCKERSERVICE in services or do_all_except_docker:
            self.toggle_docker_system()

        if ServicesEnum.DOCKERCLIENT in services or do_all_except_docker:
            self.toggle_docker_client()

        if ServicesEnum.GIT in services or do_all:
            self.toggle_git()

        if ServicesEnum.APT in services or do_all:
            self.toggle_apt()

        if ServicesEnum.ENV in services or do_all:
            self.toggle_env()

        if ServicesEnum.GRADLE in services or do_all:
            self.toggle_gradle()

# vim: ts=4 sw=4 sts=0 expandtab ff=unix :
